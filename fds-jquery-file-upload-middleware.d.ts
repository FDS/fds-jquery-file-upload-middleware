/**
 * Created by Sergey Borisov on 30.11.2015.
 */

module FDSJQueryFileUploadMiddleware
{
    import events = require('events');
    import express = require('express');
    export interface ImageSize
    {
        width:number;
        height: number;
    }
    export interface Options
    {
        tmpDir?: string;
        uploadDir?: string|Function;
        uploadUrl?: string|Function;
        targetDir?:string|Function;
        targetUrl?:string|Function;
        maxPostSize?: number;
        minFileSize?: number;
        maxFileSize?: number;
        acceptFileTypes?: RegExp;
        imageTypes?: RegExp;
        imageVersions?: { [key:string]:ImageSize; };
        imageArgs?: string[];
        accessControl?: {
            allowOrigin: string;
            allowMethods: string;
        }
    }

    export interface FileHandler extends express.RequestHandler
    {

    }

    export interface FileUpload extends events.EventEmitter
    {
        fileHandler(options: FDSJQueryFileUploadMiddleware.Options):FileHandler;
    }
}
declare module "fds-jquery-file-upload-middleware"
{
    export = middleware;
}

declare var middleware: FDSJQueryFileUploadMiddleware.FileUpload;